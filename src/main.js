const React = appdk.UI.React;

// registrr app
appdk.newApp({
    render: ({ bridge, intent, props }) => {
        React.useEffect(() => {
        }, []);

        return document.createElement({
            // element name
            tag: 'div',

            // style
            style: {
                display: 'flex',
                flexDirection: 'column',
                height: '100%',
                width: '100%',
                background: '#1def33',
                alignItems: 'center',
                justifyContent: 'center'
            },

            // element content
            children: ["PUT YOUR JSON IN THIS ARRAY AN REBUILD"]
        });
    },
    shell: {
        STATE: {},
        COMMANDS: {
            democommand: (TOOLS) => {
                TOOLS.print("THIS IS A DEMO COMMAND")
            }
        },
        BLACKLIST: [],
    }
});
